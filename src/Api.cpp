#include "../include/Api.h"
#include <iostream>

using json = nlohmann::json;

namespace weather
{
    Api::Api()
    {
        curl_global_init(CURL_GLOBAL_DEFAULT);
        curl = curl_easy_init();
        if(curl)
        {
            curl_easy_setopt(curl, CURLOPT_URL, "https://api.darksky.net/forecast/05693636686a6c5029b0356e2702f2c2/51.884762,4.550976?units=ca");
            // Only support HTTPS certificates that are valid.
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 1L);
            curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 2L);
            curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
            curl_easy_setopt(curl, CURLOPT_WRITEDATA, &read_buffer);
            // DEBUG
            curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        }
        else
        {
            fprintf(stderr, "Curl not available.");
        }
    }

    Api::~Api()
    {
        json_data = NULL;
        curl_global_cleanup();
    }

    size_t Api::write_data(void *contents, size_t size, size_t nmemb, void *userp)
    {
        // https://stackoverflow.com/a/9786295
        size_t realsize = size * nmemb;
        ((std::string*)userp)->append((char*)contents, realsize);
        return realsize;
    }

    /***
     * This gets the data from the API and writes it to cache.
     */
    int Api::get_from_api()
    {
        if(curl)
        {
            result = curl_easy_perform(curl);
            if(result != CURLE_OK)
            {
                fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(result));
            }
            //cache_helper.write_to_cache((char *)read_buffer.c_str());
            json_data = json::parse(read_buffer.c_str());
            curl_easy_cleanup(curl);
            return 0;
        }
        return 1;
    }
}
