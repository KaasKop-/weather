#include "../include/Cache.h"
#include <iostream>

using json = nlohmann::json;

namespace weather
{
    Cache::Cache()
    {
        try
        {
            // We get the temp directory from variables if that fails it defualts to /tmp.
            cache_path = boost::filesystem::temp_directory_path();
            cache_path /= "weather.json";
        } catch (boost::filesystem::filesystem_error &e)
        {
            fprintf(stderr, "Cannot find temporary directory, make sure that one of these is defined as a variable: TEMP, TMP, TMPDIR, TEMPDIR.\n");
        }
        
    }

    Cache::~Cache()
    {
    }

    int Cache::load_cache_into_memory()
    {
        // Since we'll only call the API once this will be the main point for accessing json data.
        boost::filesystem::ifstream ifs(cache_path);
        if(boost::filesystem::is_regular_file(cache_path))
        {
            //File exists we gonna fetch the API data from it now.
            ifs >> json_data;
            ifs.close();
        } else
        {
            fprintf(stderr, "Cache was not created. Cannot access cache file in your temp folder.");
            return 1;
        }
        return 0;
    }

    int Cache::write_to_cache(char* text)
    {
        boost::filesystem::ofstream ofs(cache_path);
        if(boost::filesystem::is_regular_file(cache_path))
        {
            // File was succesfully created and we're now writing the text to it.
            auto text_to_json = json::parse(text);
            // dump(4) makes it print prettified. uwu
            ofs << text_to_json.dump(4) << std::endl;
            ofs.close();
            return 0;
        } else
        {
            fprintf(stderr, "Cannot create cache file in temp directory.\n");
            return 1;
        }
        return 1;
    }

    int Cache::write_directly_to_memory(char* text)
    {
        auto text_to_json = json::parse(text);
        //json_data << text_to_json.dump(4) << std::endl;
    }
}
