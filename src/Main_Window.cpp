#include "../include/Main_Window.h"
#include "../include/Api.h"
#include <gtkmm-3.0/gtkmm/main.h>
#include <iostream>

namespace weather
{

//Placeholder values like random weather icon and -/-c until we load the actual main view into it.
    Main_Window::Main_Window() :
    temperature_now("-"),
    temperature_low("-"),
    temperature_max("-")
    {
        set_title("Weather");
        set_border_width(5);
        set_default_size(720, 1440);

        weather::Api api;
        api.get_from_api();
        
        //std::cout << api.json_data["currently"]["temperature"].get<float>() << std::endl;
        show_all_children();
    }

    Main_Window::~Main_Window()
    {
    }
}
