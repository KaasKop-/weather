#include <gtkmm-3.0/gtkmm.h>
#include <iostream>
#include <string>
#include "../include/Api.h"

int main(int argc, char *argv[])
{
    auto app = Gtk::Application::create(argc, argv, "KaasKop.Weather");
    auto weather_main = Gtk::Builder::create();
    auto api = weather::Api();
    Gtk::Window* kaas;

    try 
    {
        weather_main->add_from_file("../weather.glade");
    }
    catch(const Glib::FileError& ex)
    {
        std::cerr << "FileError: " << ex.what() << std::endl;
        return 1;
    }
    catch(const Glib::MarkupError& ex)
    {
        std::cerr << "MarkupError: " << ex.what() << std::endl;
        return 1;
    }
    catch(const Gtk::BuilderError& ex)
    {
        std::cerr << "BuilderError: " << ex.what() << std::endl;
        return 1;
    }

    weather_main->get_widget("MainWindow", kaas);
    if(kaas)
    {
        if(api.get_from_api() == 0)
        {
            Gtk::Label* temperature_data = nullptr;
            Gtk::Image* current_weather_icon = nullptr;

            // Append the number of the day to this and then we have the ID
            std::string weather_temperature_id = "weather_forecast_day";
            std::string weather_forecast_id = "weather_forecast_icon_day";

            weather_main->get_widget("weather_today", temperature_data);
            weather_main->get_widget("weather_today_icon", current_weather_icon);
            temperature_data->set_text(std::to_string(api.json_data["currently"]["temperature"].get<int>()) + "˚");
            // 7 iterations since thats the max darsky looks forward and we can set the labels
            for(int i = 1; i <= 7; i++)
            {
                weather_main->get_widget(weather_temperature_id + std::to_string(i), temperature_data);
                temperature_data->set_text(std::to_string(api.json_data["daily"]["data"][i]["temperatureLow"].get<int>()) + "˚ / " + std::to_string(api.json_data["daily"]["data"][i]["temperatureHigh"].get<int>()) + "˚");
            }
        }
        app->run(*kaas);
    }
    delete kaas;
    return 0;
}
