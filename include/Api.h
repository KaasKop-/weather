// This will make calls to an API (Darksky) and fetch the weather data and represent it for the windows properly
#ifndef API_H
#define API_H
#include <curl/curl.h>
#include <string>
#include <nlohmann/json.hpp>

namespace weather
{
    class Api
    {
        CURL *curl;
        CURLcode result;
    public:
        Api();
        ~Api();
        int get_from_api();
        static size_t write_data(void *contents, size_t size, size_t nmemb, void *userp);
        //Cache cache_helper;
        nlohmann::json json_data;
    private:
        std::string read_buffer;
    };
}

#endif