#ifndef CACHE_H
#define CACHE_H
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <nlohmann/json.hpp>

namespace weather
{
    class Cache
    {
        boost::filesystem::path cache_path;
    public:
        Cache();
        ~Cache();
        int write_to_cache(char* text);
        int load_cache_into_memory();
        int write_directly_to_memory(char* text);
    public:
        nlohmann::json json_data;
    };
}

#endif