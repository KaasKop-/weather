#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <gtkmm-3.0/gtkmm/window.h>
#include <gtkmm-3.0/gtkmm/label.h>
#include <gtkmm-3.0/gtkmm/spinner.h>
#include <gtkmm-3.0/gtkmm/grid.h>

namespace weather
{
    class Main_Window : public Gtk::Window
    {
    public:
        Main_Window();
        ~Main_Window();
    private:
        Gtk::Window* window;
        Gtk::Grid main_grid;

        // text in the interface
        Gtk::Label temperature_now;
        Gtk::Label temperature_max;
        Gtk::Label temperature_low;

        Gtk::Label temperature_forecast_low;
        Gtk::Label temperature_forecase_high;
    };
}

#endif
